# EcommerceUdemy

Site type E-commerce conçu à titre éducatif.

Ce site est un site de vente de vêtements.

## Environnement de développement



### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec ma commande suivante (de la Symfony CLI) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
docker-compose up -d
symfony serve -d
```

## Lancer des tests

```bash
php bin/phpunit --testdox
```