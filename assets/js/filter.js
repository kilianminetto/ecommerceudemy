export default function filterForm(input, label) {
    input.forEach(e => {
        e.classList.add('form-check-input')
    });

    label.forEach(e => {
        e.classList.add('form-check-label');
        e.classList.add('d-block');
    });
}