<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Entity\Adresse;
use App\Form\AdressesType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountAdresseController extends AbstractController
{
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/compte/adresse", name="account_adresse")
     */
    public function index(): Response
    {
        return $this->render('account/adresse.html.twig');
    }

    /**
     * @Route("/compte/ajouter-une-adresse", name="account_adresse_add")
     * @param Request $request
     * @param Cart $cart
     * @return Response
     */
    public function add(Request $request, Cart $cart): Response
    {
        $adresse = new Adresse();
        $form = $this->createForm(AdressesType::class, $adresse);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adresse->setUser($this->getUser());

            $this->manager->persist($adresse);
            $this->manager->flush();

            $this->addFlash(
                'notice',
                'Votre adresse a bien été enregistrée'
            );

            if ($cart->get()) {
                return $this->redirectToRoute('order');
            }
            return $this->render('account/adresse.html.twig');
        }

        return $this->render('account/adresse_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/compte/modifier-une-adresse/{id}", name="account_adresse_edit")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function edit(Request $request, $id): Response
    {
        $adresse = $this->manager->getRepository(Adresse::class)->findOneById($id);

        if (!$adresse || $adresse->getUser() !== $this->getUser()) {
            return $this->redirectToRoute('account_adresse');
        }

        $form = $this->createForm(AdressesType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->flush();

            $this->addFlash(
                'notice',
                'Votre adresse a bien été modifiée'
            );

            $this->redirectToRoute('account_adresse');
        }

        return $this->render('account/adresse_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/compte/supprimer-une-adresse/{id}", name="account_adresse_delete")
     * @param $id
     * @return Response
     */
    public function delete($id): Response
    {
        $adresse = $this->manager->getRepository(Adresse::class)->findOneById($id);

        if ($adresse || $adresse->getUser() === $this->getUser()) {
            $this->manager->remove($adresse);
            $this->manager->flush();

            $this->addFlash(
                'notice',
                'Votre adresse a bien été suprimée'
            );
        }
        return $this->redirectToRoute('account_adresse');
    }
}
