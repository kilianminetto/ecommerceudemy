<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/inscription", name="register")
     * @param Request $request
     * @param UserPasswordHasherInterface $hasher
     * @return Response
     */
    public function index(Request $request, UserPasswordHasherInterface $hasher): Response
    {
        $notification = null;

        $user = new User();
        $form = $this->createForm(
            RegisterType::class,
            $user
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $searchEmail = $this->manager->getRepository(User::class)->findOneByEmail($user->getEmail());

            if (!$searchEmail) {
                $password = $hasher->hashPassword(
                    $user,
                    $user->getPassword()
                );
                $user->setPassword($password);

                $this->manager->persist($user);
                $this->manager->flush();

                $this->addFlash(
                    'notice',
                    'Votre inscription s\'est correctement déroulée. 
                    Vous pouvez dès à présent vous connecter à votre compte.'
                );

                $this->redirectToRoute('app_login');
            } else {
                $this->addFlash(
                    'error',
                    'L\'email que vous avez renseigné existe déjà'
                );
            }
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
