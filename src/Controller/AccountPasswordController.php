<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class AccountPasswordController extends AbstractController
{
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/account/mot-de-passe", name="account_password")
     * @param Request $request
     * @param UserPasswordHasherInterface $hasher
     * @return Response
     */
    public function index(Request $request, UserPasswordHasherInterface $hasher): Response
    {
        $notification = null;
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $form->get('old_password')->getData();

            if ($hasher->isPasswordValid($user, $oldPassword)) {
                $newPassword = $form->get('new_password')->getData();
                $password = $hasher->hashPassword($user, $newPassword);

                $user->setPassword($password);

                $this->manager->flush();
                $notification = 'Votre mot de passe a bien été modifié';
            } else {
                $notification = 'Un problème est survenu lors du changement de mot de passe !';
            }
        }

        return $this->render('account/password.html.twig', [
            'form' =>$form->createView(),
            'notification' => $notification
        ]);
    }
}
