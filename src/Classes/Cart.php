<?php


namespace App\Classes;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Cart
{
    private SessionInterface $session;
    private EntityManagerInterface $manager;

    public function __construct(SessionInterface $session, EntityManagerInterface $manager)
    {
        $this->session = $session;
        $this->manager = $manager;
    }

    /**
     * Fonction permettant de récupérer le panier de l'utilisateur
     * @return array
     */
    public function getFull(): array
    {
        $cartComplete = array();

        if ($this->get()) {
            foreach ($this->get() as $id => $quantity) {
                $producObject = $this->manager->getRepository(Product::class)->findOneById($id);

                if (!$producObject) {
                    $this->delete($id);
                    continue;
                }
                $cartComplete[] = [
                    'product' => $producObject,
                    'quantity' => $quantity
                ];
            }
        }
        return $cartComplete;
    }

    /**
     * Fonction permmettant d'ajouter un produit ou la quantité de produit
     * (s'il en existe déjà)
     * @param $id
     */
    public function add($id): void
    {
        $cart = $this->session->get('cart', []);

        if (!empty($cart[$id])) {
            $cart[$id]++;
        } else {
            $cart[$id] = 1;
        }
        $this->session->set('cart', $cart);
    }

    /**
     * Fonction permettant de récupérer l'état du panier dans la session
     * @return mixed
     */
    public function get()
    {
        return $this->session->get('cart');
    }

    /**
     * Fonction permettant de supprimer l'état du panier dans la session
     * @return mixed
     */
    public function remove()
    {
        return $this->session->remove('cart');
    }

    /**
     * Fonction permettant de un produit du panier
     * @param $id
     */
    public function delete($id)
    {
        $cart = $this->session->get('cart', []);
        unset($cart[$id]);
        return $this->session->set('cart', $cart);
    }

    /**
     * Fonction permettant de retirer une quantité au produit dans le panier
     * @param $id
     * @return mixed
     */
    public function takeOf($id)
    {
        $cart = $this->session->get('cart', []);

        if ($cart[$id] > 1) {
            $cart[$id]--;
        } else {
            unset($cart[$id]);
        }
        return $this->session->set('cart', $cart);
    }
}
