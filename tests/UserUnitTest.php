<?php

namespace App\Tests;

use App\Entity\Adresse;
use App\Entity\Order;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mime\Address;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();
        $address = new Adresse();
        $order = new Order();

        $user       ->setEmail('true@test.com')
                    ->setFirstname('firstname')
                    ->setLastname('lastname')
                    ->setPassword('password')
                    ->addAdress($address)
                    ->addOrder($order);

        $this->assertSame($user->getEmail(), 'true@test.com');
        $this->assertSame($user->getFirstname(), 'firstname');
        $this->assertSame($user->getLastname(), 'lastname');
        $this->assertSame($user->getPassword(), 'password');
        $this->assertContains($address, $user->getAdresses());
        $this->assertContains($order, $user->getOrders());
    }

    public function testIsFalse(): void
    {
        $user = new User();
        $address = new Adresse();
        $order = new Order();

        $user       ->setEmail('true@test.com')
                    ->setFirstname('firstname')
                    ->setLastname('lastname')
                    ->setPassword('password')
                    ->addAdress($address)
                    ->addOrder($order);

        $this->assertNotSame($user->getEmail(), 'false@test.com');
        $this->assertNotSame($user->getFirstname(), 'false');
        $this->assertNotSame($user->getLastname(), 'false');
        $this->assertNotSame($user->getPassword(), 'false');
        $this->assertNotContains(new Adresse(), $user->getAdresses());
        $this->assertNotContains(new Order(), $user->getOrders());
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getAdresses());
        $this->assertEmpty($user->getOrders());
    }
}
