<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ProductUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $product = new Product();
        $category = new Category();

        $product        ->setName('name')
                        ->setSlug('slug')
                        ->setIllustration('illustration')
                        ->setSubtitle('subtitle')
                        ->setDescription('description')
                        ->setPrice(3.8)
                        ->setIsBest(true);

        $this->assertSame($product->getName(), 'name');
        $this->assertSame($product->getSlug(), 'slug');
        $this->assertSame($product->getIllustration(), 'illustration');
        $this->assertSame($product->getSubtitle(), 'subtitle');
        $this->assertSame($product->getDescription(), 'description');
        $this->assertSame($product->getPrice(), 3.8);
        $this->assertTrue($product->getIsBest());
    }

    public function testIsFalse(): void
    {
        $product = new Product();

        $product        ->setName('name')
                        ->setSlug('slug')
                        ->setIllustration('illustration')
                        ->setSubtitle('subtitle')
                        ->setDescription('description')
                        ->setPrice(3.8)
                        ->setIsBest(true);

        $this->assertNotSame($product->getName(), 'false');
        $this->assertNotSame($product->getSlug(), 'false');
        $this->assertNotSame($product->getIllustration(), 'false');
        $this->assertNotSame($product->getSubtitle(), 'false');
        $this->assertNotSame($product->getDescription(), 'false');
        $this->assertNotSame($product->getPrice(), 5.8);
        $this->assertNotFalse($product->getIsBest());
    }

    public function testIsEmpty(): void
    {
        $product = new Product();

        $this->assertEmpty($product->getName());
        $this->assertEmpty($product->getSlug());
        $this->assertEmpty($product->getIllustration());
        $this->assertEmpty($product->getSubtitle());
        $this->assertEmpty($product->getDescription());
        $this->assertEmpty($product->getPrice());
        $this->assertEmpty($product->getIsBest());
    }
}
