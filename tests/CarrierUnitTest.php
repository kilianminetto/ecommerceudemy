<?php

namespace App\Tests;

use App\Entity\Carrier;
use PHPUnit\Framework\TestCase;

class CarrierUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $carrier = new Carrier();

        $carrier        ->setName('name')
                        ->setDescription('description')
                        ->setPrice(3.8);

        $this->assertSame($carrier->getName(), 'name');
        $this->assertSame($carrier->getDescription(), 'description');
        $this->assertSame($carrier->getPrice(), 3.8);
    }

    public function testIsFalse(): void
    {
        $carrier = new Carrier();

        $carrier        ->setName('name')
                        ->setDescription('description')
                        ->setPrice(3.8);

        $this->assertNotSame($carrier->getName(), 'false');
        $this->assertNotSame($carrier->getDescription(), 'false');
        $this->assertNotSame($carrier->getPrice(), 5.8);
    }

    public function testIsEmpty(): void
    {
        $carrier = new Carrier();

        $this->assertEmpty($carrier->getName());
        $this->assertEmpty($carrier->getDescription());
        $this->assertEmpty($carrier->getPrice());
    }
}
