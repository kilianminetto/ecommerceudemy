<?php

namespace App\Tests;

use App\Entity\Header;
use PHPUnit\Framework\TestCase;

class HeaderUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $header = new Header();

        $header     ->setTitle('title')
                    ->setContent('content')
                    ->setBtnTitle('btnTitle')
                    ->setBtnUrl('btnUrl')
                    ->setIllustration('illustration');

        $this->assertSame($header->getTitle(), 'title');
        $this->assertSame($header->getContent(), 'content');
        $this->assertSame($header->getBtnTitle(), 'btnTitle');
        $this->assertSame($header->getBtnUrl(), 'btnUrl');
        $this->assertSame($header->getIllustration(), 'illustration');
    }

    public function testIsFalse(): void
    {
        $header = new Header();

        $header     ->setTitle('title')
                    ->setContent('content')
                    ->setBtnTitle('btnTitle')
                    ->setBtnUrl('btnUrl')
                    ->setIllustration('illustration');

        $this->assertNotSame($header->getTitle(), 'false');
        $this->assertNotSame($header->getContent(), 'false');
        $this->assertNotSame($header->getBtnTitle(), 'false');
        $this->assertNotSame($header->getBtnUrl(), 'false');
        $this->assertNotSame($header->getIllustration(), 'false');
    }

    public function testIsEmpty(): void
    {
        $header = new Header();

        $this->assertEmpty($header->getTitle());
        $this->assertEmpty($header->getContent());
        $this->assertEmpty($header->getBtnTitle());
        $this->assertEmpty($header->getBtnUrl());
        $this->assertEmpty($header->getIllustration());
    }
}
