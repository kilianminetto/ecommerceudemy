<?php

namespace App\Tests;

use App\Entity\OrderDetails;
use PHPUnit\Framework\TestCase;

class OrderDetailsUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $orderDetails = new OrderDetails();

        $orderDetails       ->setProduct('product')
                            ->setQuantity(1)
                            ->setPrice(3.58)
                            ->setTotal(3.58);

        $this->assertSame($orderDetails->getProduct(), 'product');
        $this->assertSame($orderDetails->getQuantity(), 1);
        $this->assertSame($orderDetails->getPrice(), 3.58);
        $this->assertSame($orderDetails->getTotal(), 3.58);
    }

    public function testIsFalse(): void
    {
        $orderDetails = new OrderDetails();

        $orderDetails       ->setProduct('product')
                            ->setQuantity(1)
                            ->setPrice(3.58)
                            ->setTotal(3.58);

        $this->assertNotSame($orderDetails->getProduct(), 'false');
        $this->assertNotSame($orderDetails->getQuantity(), 5);
        $this->assertNotSame($orderDetails->getPrice(), 5.58);
        $this->assertNotSame($orderDetails->getTotal(), 5.58);
    }

    public function testIsEmpty(): void
    {
        $orderDetails = new OrderDetails();

        $this->assertEmpty($orderDetails->getProduct());
        $this->assertEmpty($orderDetails->getQuantity());
        $this->assertEmpty($orderDetails->getPrice());
        $this->assertEmpty($orderDetails->getTotal());
    }
}
