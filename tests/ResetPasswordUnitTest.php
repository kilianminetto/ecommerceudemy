<?php

namespace App\Tests;

use App\Entity\ResetPassword;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class ResetPasswordUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $resetPassword = new ResetPassword();
        $user = new User();
        $datetime = new \DateTime();

        $resetPassword      ->setUser($user)
                            ->setToken('token')
                            ->setCreatedAt($datetime);

        $this->assertSame($resetPassword->getUser(), $user);
        $this->assertSame($resetPassword->getToken(), 'token');
        $this->assertSame($resetPassword->getCreatedAt(), $datetime);
    }

    public function testIsFalse(): void
    {
        $resetPassword = new ResetPassword();
        $user = new User();
        $datetime = new \DateTime();

        $resetPassword      ->setUser($user)
                            ->setToken('token')
                            ->setCreatedAt($datetime);

        $this->assertNotSame($resetPassword->getUser(), new User());
        $this->assertNotSame($resetPassword->getToken(), 'false');
        $this->assertNotSame($resetPassword->getCreatedAt(), new \DateTime());
    }

    public function testIsEmpty(): void
    {
        $resetPassword = new ResetPassword();

        $this->assertEmpty($resetPassword->getUser());
        $this->assertEmpty($resetPassword->getToken());
        $this->assertEmpty($resetPassword->getCreatedAt());
    }
}
