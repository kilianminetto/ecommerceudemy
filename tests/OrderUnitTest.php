<?php

namespace App\Tests;

use App\Entity\Order;
use App\Entity\OrderDetails;
use PHPUnit\Framework\TestCase;

class OrderUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $order = new Order();
        $orderDetails = new OrderDetails();
        $datetime = new \DateTime();

        $order      ->setCreatedAt($datetime)
                    ->setCarrierName('carrierName')
                    ->setCarrierPrice(3.58)
                    ->setDelivery('delivery')
                    ->setReference('reference')
                    ->setStripeSessionId('stripeSessionId')
                    ->setState(1)
                    ->addOrderDetail($orderDetails);

        $this->assertSame($order->getCreatedAt(), $datetime);
        $this->assertSame($order->getCarrierName(), 'carrierName');
        $this->assertSame($order->getCarrierPrice(), 3.58);
        $this->assertSame($order->getDelivery(),'delivery');
        $this->assertSame($order->getReference(), 'reference');
        $this->assertSame($order->getStripeSessionId(), 'stripeSessionId');
        $this->assertSame($order->getState(), 1);
        $this->assertContains($orderDetails, $order->getOrderDetails());
    }

    public function testIsFalse(): void
    {
        $order = new Order();
        $orderDetails = new OrderDetails();
        $datetime = new \DateTime();

        $order      ->setCreatedAt($datetime)
                    ->setCarrierName('carrierName')
                    ->setCarrierPrice(3.58)
                    ->setDelivery('delivery')
                    ->setReference('reference')
                    ->setStripeSessionId('stripeSessionId')
                    ->setState(1)
                    ->addOrderDetail($orderDetails);

        $this->assertNotSame($order->getCreatedAt(), new \DateTime());
        $this->assertNotSame($order->getCarrierName(), 'false');
        $this->assertNotSame($order->getCarrierPrice(), 5.58);
        $this->assertNotSame($order->getDelivery(),'false');
        $this->assertNotSame($order->getReference(), 'false');
        $this->assertNotSame($order->getStripeSessionId(), 'false');
        $this->assertNotSame($order->getState(), 5);
        $this->assertNotContains(new OrderDetails(), $order->getOrderDetails());
    }

    public function testIsEmpty(): void
    {
        $order = new Order();

        $this->assertEmpty($order->getCreatedAt());
        $this->assertEmpty($order->getCarrierName());
        $this->assertEmpty($order->getCarrierPrice());
        $this->assertEmpty($order->getDelivery());
        $this->assertEmpty($order->getReference());
        $this->assertEmpty($order->getStripeSessionId());
        $this->assertEmpty($order->getState());
        $this->assertEmpty($order->getOrderDetails());
    }
}
