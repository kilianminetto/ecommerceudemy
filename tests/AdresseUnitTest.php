<?php

namespace App\Tests;

use App\Entity\Adresse;
use PHPUnit\Framework\TestCase;

class AdresseUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $address = new Adresse();

        $address        ->setName('name')
                        ->setFirstname('firstname')
                        ->setLastname('lastname')
                        ->setCompany('company')
                        ->setAdress('address')
                        ->setPostal('postal')
                        ->setCity('city')
                        ->setCountry('country')
                        ->setPhone('phone');

        $this->assertSame($address->getName(), 'name');
        $this->assertSame($address->getFirstname(), 'firstname');
        $this->assertSame($address->getLastname(), 'lastname');
        $this->assertSame($address->getCompany(), 'company');
        $this->assertSame($address->getAdress(), 'address');
        $this->assertSame($address->getPostal(), 'postal');
        $this->assertSame($address->getCity(), 'city');
        $this->assertSame($address->getCountry(), 'country');
        $this->assertSame($address->getPhone(), 'phone');
    }

    public function testIsFalse(): void
    {
        $address = new Adresse();

        $address        ->setName('name')
                        ->setFirstname('firstname')
                        ->setLastname('lastname')
                        ->setCompany('company')
                        ->setAdress('address')
                        ->setPostal('postal')
                        ->setCity('city')
                        ->setCountry('country')
                        ->setPhone('phone');

        $this->assertNotSame($address->getName(), 'false');
        $this->assertNotSame($address->getFirstname(), 'false');
        $this->assertNotSame($address->getLastname(), 'false');
        $this->assertNotSame($address->getCompany(), 'false');
        $this->assertNotSame($address->getAdress(), 'false');
        $this->assertNotSame($address->getPostal(), 'false');
        $this->assertNotSame($address->getCity(), 'false');
        $this->assertNotSame($address->getCountry(), 'false');
        $this->assertNotSame($address->getPhone(), 'false');
    }

    public function testIsEmpty(): void
    {
        $address = new Adresse();

        $this->assertEmpty($address->getName());
        $this->assertEmpty($address->getFirstname());
        $this->assertEmpty($address->getLastname());
        $this->assertEmpty($address->getCompany());
        $this->assertEmpty($address->getAdress());
        $this->assertEmpty($address->getPostal());
        $this->assertEmpty($address->getCity());
        $this->assertEmpty($address->getCountry());
        $this->assertEmpty($address->getPhone());
    }
}
